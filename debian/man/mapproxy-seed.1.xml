<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE refentry PUBLIC "-//OASIS//DTD DocBook XML V4.4//EN" "http://www.oasis-open.org/docbook/xml/4.4/docbookx.dtd">
<refentry id='mapproxy-seed'>

  <refmeta>
    <refentrytitle>mapproxy-seed</refentrytitle>
    <manvolnum>1</manvolnum>
  </refmeta>

  <refnamediv>
    <refname>mapproxy-seed</refname>
    <refpurpose>pre-generate tiles for MapProxy</refpurpose>
  </refnamediv>

  <refsynopsisdiv id='synopsis'>
    <cmdsynopsis>
      <command>mapproxy-seed</command>
      <arg choice='opt'><replaceable>OPTIONS</replaceable></arg>
    </cmdsynopsis>
  </refsynopsisdiv>

  <refsect1 id='description'>
    <title>DESCRIPTION</title>
    <para>
      <command>mapproxy-seed</command> pre-generates tiles for MapProxy to
      improve the performance for commonly requested views, because MapProxy
      creates all tiles on demand otherwise.
    </para>
    <para>
      The tool can seed one or more polygon or BBOX areas for each cached
      layer.
    </para>
    <para>
      MapProxy does not seed the tile pyramid level by level, but traverses
      the tile pyramid depth-first, from bottom to top.
      This is optimized to work with the caches of your operating system and
      geospatial database, and not against.
    </para>
  </refsect1>

  <refsect1 id='options'>
    <title>OPTIONS</title>
    <variablelist>

      <varlistentry>
        <term><option>-s</option> <replaceable>seed.yaml</replaceable></term>
        <term><option>--seed-conf</option> <replaceable>seed.yaml</replaceable></term>
        <listitem>
          <para>
            The seed configuration.
            You can also pass the configuration as the last argument to
            <command>mapproxy-seed</command>.
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-f</option> <replaceable>mapproxy.yaml</replaceable></term>
        <term><option>--proxy-conf</option> <replaceable>mapproxy.yaml</replaceable></term>
        <listitem>
          <para>
            The MapProxy configuration to use.
            This file should describe all caches and grids that the seed
            configuration references.
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-c</option> <replaceable>N</replaceable></term>
        <term><option>--concurrency</option> <replaceable>N</replaceable></term>
        <listitem>
          <para>
            The number of concurrent seed worker.
            Some parts of the seed tool are CPU intensive (image splitting and
            encoding), use this option to distribute that load across multiple
            CPUs.
            To limit the concurrent requests to the source WMS see
            <literal>wms_source_concurrent_requests_label</literal>.
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-n</option></term>
        <term><option>--dry-run</option></term>
        <listitem>
          <para>
            This will simulate the seed/cleanup process without requesting,
            creating or removing any tiles.
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>--summary</option></term>
        <listitem>
          <para>
            Print a summary of all seeding and cleanup tasks and exit.
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-i</option></term>
        <term><option>--interactive</option></term>
        <listitem>
          <para>
            Print a summary of each seeding and cleanup task and ask if
            <command>mapproxy-seed</command> should seed/cleanup that task.
            It will query for each task before it starts.
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>--seed</option> <replaceable>task1,task2,..</replaceable></term>
        <listitem>
          <para>
            Only seed the named seeding tasks.
            You can select multiple tasks with a list of comma separated names,
            or you can use the <option>--seed</option> option multiple times.
            You can use <literal>ALL</literal> to select all tasks.
            This disables all cleanup tasks unless you also use the
            <option>--cleanup</option> option.
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>--cleanup</option> <replaceable>task1,task2,..</replaceable></term>
        <listitem>
          <para>
            Only cleanup the named tasks.
            You can select multiple tasks with a list of comma separated names,
            or you can use the <option>--cleanup</option> option multiple times.
            You can use <literal>ALL</literal> to select all tasks.
            This disables all seeding tasks unless you also use the
            <option>--seed</option> option.
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>--continue</option></term>
        <listitem>
          <para>
            Continue an interrupted seed progress.
            MapProxy will start the seeding progress at the beginning if the
            progress file (<option>--progress-file</option>) was not found.
            MapProxy can only continue if the previous seed was started with
            the <option>--progress-file</option> or
            <option>--continue</option> option.
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>--progress-file</option></term>
        <listitem>
          <para>
            Filename where MapProxy stores the seeding progress for the
            <option>--continue</option> option.
            Defaults to <literal>.mapproxy_seed_progress</literal> in the
            current working directory.
            MapProxy will remove that file after a successful seed.
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>--duration</option></term>
        <listitem>
          <para>
            Stop seeding process after this duration.
            This option accepts duration in the following format:
            120s, 15m, 4h, 0.5d
            Use this option in combination with <option>--continue</option> to
            be able to resume the seeding.
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>--reseed-file</option></term>
        <listitem>
          <para>
            File created by <command>mapproxy-seed</command> at the start of a
            new seeding.
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>--reseed-interval</option></term>
        <listitem>
          <para>
            Only start seeding if <option>--reseed-file</option> is older then
            this duration.
            This option accepts duration in the following format:
            120s, 15m, 4h, 0.5d
            Use this option in combination with <option>--continue</option> to
            be able to resume the seeding.
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>--use-cache-lock</option></term>
        <listitem>
          <para>
            Lock each cache to prevent multiple parallel
            <command>mapproxy-seed</command> calls to work on the same cache.
            It does not lock normal operation of MapProxy.
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>--log-config</option></term>
        <listitem>
          <para>
            The logging configuration file to use.
          </para>
        </listitem>
      </varlistentry>

    </variablelist>

  </refsect1>
  
  <refsect1 id='example'>
    <title>Example</title>
    <para>
      Seed with concurrency of 4:
    </para>
    <screen>
<command>mapproxy-seed</command> <option>-f</option> mapproxy.yaml <option>-c</option> 4 seed.yaml
    </screen>
    <para>
      Print summary of all seed tasks and exit:
    </para>
    <screen>
<command>mapproxy-seed</command> <option>-f</option> mapproxy.yaml <option>-s</option> seed.yaml <option>--summary</option> <option>--seed</option> ALL
    </screen>
    <para>
      Interactively select which tasks should be seeded:
    </para>
    <screen>
<command>mapproxy-seed</command> <option>-f</option> mapproxy.yaml <option>-s</option> seed.yaml <option>-i</option>
    </screen>
    <para>
      Seed task1 and task2 and cleanup task3 with concurrency of 2:
    </para>
    <screen>
<command>mapproxy-seed</command> <option>-f</option> mapproxy.yaml <option>-s</option> seed.yaml <option>-c</option> 2 <option>--seed</option> task1,task2 \
 <option>--cleanup</option> task3
    </screen>
  </refsect1>

</refentry>
