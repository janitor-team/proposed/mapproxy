<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE refentry PUBLIC "-//OASIS//DTD DocBook XML V4.4//EN" "http://www.oasis-open.org/docbook/xml/4.4/docbookx.dtd">
<refentry id='mapproxy-util-autoconfig'>

  <refmeta>
    <refentrytitle>mapproxy-util-autoconfig</refentrytitle>
    <manvolnum>1</manvolnum>
  </refmeta>

  <refnamediv>
    <refname>mapproxy-util-autoconfig</refname>
    <refpurpose>creates MapProxy and MapProxy-seeding configurations</refpurpose>
  </refnamediv>

  <refsynopsisdiv id='synopsis'>
    <cmdsynopsis>
      <command>mapproxy-util autoconfig</command>
      <arg choice='opt'><replaceable>OPTIONS</replaceable></arg>
    </cmdsynopsis>
  </refsynopsisdiv>

  <refsect1 id='description'>
    <title>DESCRIPTION</title>
    <para>
      <command>mapproxy-util autoconfig</command> creates MapProxy and
      MapProxy-seeding configurations based on existing WMS capabilities
      documents.
    </para>
    <para>
      It creates a <literal>source</literal> for each available layer.
      The source will include a BBOX coverage from the layer extent,
      <literal>legendurl</literal> for legend graphics,
      <literal>featureinfo</literal> for querlyable layers,
      scale hints and all detected <literal>supported_srs</literal>.
      It will duplicate the layer tree to the <literal>layers</literal>
      section of the MapProxy configuration, including the name, title and
      abstract.
    </para>
    <para>
      The tool will create a cache for each source layer and
      <literal>supported_srs</literal> if there is a grid configured in your
      <option>--base</option> configuration for that SRS.
    </para>
    <para>
      The MapProxy layers will use the caches when available, otherwise they
      will use the source directly (cascaded WMS).
    </para>
    <note>
      <title>Note</title>
      <para>
        The tool can help you to create new configations, but it can’t
        predict how you will use the MapProxy services.
        The generated configuration can be highly inefficient, especially when
        multiple layers with separate caches are requested at once.
        Please make sure you understand the configuration and check the
        documentation for more options that are useful for your use-cases.
      </para>
    </note>
  </refsect1>

  <refsect1 id='options'>
    <title>OPTIONS</title>
    <variablelist>

      <varlistentry>
        <term><option>--capabilities</option> <replaceable>url|filename</replaceable></term>
        <listitem>
          <para>
            URL or filename of the WMS capabilities document.
           The tool will add <literal>REQUEST</literal> and
           <literal>SERVICE</literal> parameters to the URL as necessary.
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>--output</option> <replaceable>filename</replaceable></term>
        <listitem>
          <para>
            Filename for the created MapProxy configuration.
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>--output-seed</option> <replaceable>filename</replaceable></term>
        <listitem>
          <para>
            Filename for the created MapProxy-seeding configuration.
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>--force</option></term>
        <listitem>
          <para>
            Overwrite any existing configuration with the same output filename.
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>--base</option> <replaceable>filename</replaceable></term>
        <listitem>
          <para>
            Base configuration that should be included in the
            <option>--output</option> file with the
            <literal>base</literal> option.
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>--overwrite</option> <replaceable>filename</replaceable></term>
        <term><option>--overwrite-seed</option> <replaceable>filename</replaceable></term>
        <listitem>
          <para>
            YAML configuration that overwrites configuration options before
            the generated configuration is written to
            <option>--output</option>/<option>--output-seed</option>.
          </para>
        </listitem>
      </varlistentry>

    </variablelist>

    <refsect2 id='options-example'>
      <title>Example</title>
      <para>
        Print configuration on console:
      </para>
      <screen>
<command>mapproxy-util autoconfig</command> \
    <option>--capabilities</option> http://osm.omniscale.net/proxy/service
      </screen>
      <para>
        Write MapProxy and MapProxy-seeding configuration to files:
      </para>
      <screen>
<command>mapproxy-util autoconfig</command> \
    <option>--capabilities</option> http://osm.omniscale.net/proxy/service \
    <option>--output</option> mapproxy.yaml \
    <option>--output</option><option>-seed</option> seed.yaml
      </screen>
      <para>
        Write MapProxy configuration with caches for grids from
        <filename>base.yaml</filename>:
      </para>
      <screen>
<command>mapproxy-util autoconfig</command> \
    <option>--capabilities</option> http://osm.omniscale.net/proxy/service \
    <option>--output</option> mapproxy.yaml \
    <option>--base</option> base.yaml
      </screen>
    </refsect2>

  </refsect1>

  <refsect1 id='overwrites'>
    <title>Overwrites</title>
    <para>
      It's likely that you need to tweak the created configuration - e.g. to
      define another coverage, disable featureinfo, etc.
      You can do this by editing the output file of course, or you can modify
      the output by defining all changes to an overwrite file.
      Overwrite files are applied every time you call
      <command>mapproxy-util autoconfig</command>.
    </para>
    <para>
      Overwrites are YAML files that will be merged with the created
      configuration file.
    </para>
    <para>
      The overwrites are applied independently for each
      <literal>services</literal>, <literal>sources</literal>,
      <literal>caches</literal> and <literal>layers</literal> section.
      That means, for example, that you can modify the
      <literal>supported_srs</literal> of a source and the tool will use the
      updated SRS list to decide which caches will be configured for that
      source.
    </para>

    <refsect2 id='overwrites-example'>
      <title>Example</title>
      <para>
        Created configuration:
      </para>
      <screen>
sources:
  mysource_wms:
    type: wms
    req:
        url: http://example.org
        layers: a
      </screen>
      <para>
         Overwrite file:
      </para>
      <screen>
sources:
  mysource_wms:
    supported_srs: ['EPSG:4326'] # add new value for mysource_wms
    req:
        layers: a,b  # overwrite existing value
        custom_param: 42  #  new value
      </screen>
      <para>
         Actual configuration written to <option>--output</option>:
      </para>
      <screen>
sources:
  mysource_wms:
    type: wms
    supported_srs: ['EPSG:4326']
    req:
        url: http://example.org
        layers: a,b
        custom_param: 42
      </screen>
    </refsect2>

    <refsect2 id='overwrites-special-keys'>
      <title>Special keys</title>
      <para>
        There are a few special keys that you can use in your overwrite file.
      </para>
      <variablelist>

        <varlistentry>
          <term>All</term>
          <listitem>
            <para>
              The value of the <literal>__all__</literal> key will be merged
              into all dictionaries.
              The following overwrite will add <literal>sessionid</literal> to
              the <literal>req</literal> options of all
              <literal>sources</literal>:
              <screen>
sources:
  __all__:
    req:
      sessionid: 123456789
              </screen>
            </para>
          </listitem>
        </varlistentry>

        <varlistentry>
          <term>Extend</term>
          <listitem>
            <para>
              The values of keys ending with <literal>__extend__</literal>
              will be added to existing lists.
            </para>
            <para>
              To add another SRS for one source:
              <screen>
sources:
    my_wms:
      supported_srs__extend__: ['EPSG:31467']
              </screen>
            </para>
          </listitem>
        </varlistentry>

        <varlistentry>
          <term>Wildcard</term>
          <listitem>
            <para>
              The values of keys starting or ending with three underscores
              (<literal>___</literal>) will be merged with values where the
              key matches the suffix or prefix.
            </para>
            <para>
              For example, to set <literal>levels</literal> for
              <literal>osm_webmercator</literal> and
              <literal>aerial_webmercator</literal> and to set
              <literal>refresh_before</literal> for
              <literal>osm_webmercator</literal> and
              <literal>osm_utm32</literal>:
              <screen>
seeds:
    ____webmercator:
        levels:
          from: 0
          to: 12

    osm____:
        refresh_before:
            days: 5
              </screen>
            </para>
          </listitem>
        </varlistentry>

      </variablelist>
    </refsect2>

  </refsect1>

</refentry>
