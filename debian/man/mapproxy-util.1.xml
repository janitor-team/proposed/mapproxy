<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE refentry PUBLIC "-//OASIS//DTD DocBook XML V4.4//EN" "http://www.oasis-open.org/docbook/xml/4.4/docbookx.dtd">
<refentry id='mapproxy-util'>

  <refmeta>
    <refentrytitle>mapproxy-util</refentrytitle>
    <manvolnum>1</manvolnum>
  </refmeta>

  <refnamediv>
    <refname>mapproxy-util</refname>
    <refpurpose>commandline tool for MapProxy</refpurpose>
  </refnamediv>

  <refsynopsisdiv id='synopsis'>
    <cmdsynopsis>
      <command>mapproxy-util</command>
      <command><replaceable>subcommand</replaceable></command>
      <arg choice='opt'><replaceable>OPTIONS</replaceable></arg>
    </cmdsynopsis>
  </refsynopsisdiv>

  <refsect1 id='description'>
    <title>DESCRIPTION</title>
    <para>
      <command>mapproxy-util</command> provides sub-commands that are helpful
      when working with MapProxy.
    </para>
    <para>
      To get a list of all sub-commands call:
      <screen>
<command>mapproxy-util</command>
      </screen>
    </para>
    <para>
      To call a sub-command:
      <screen>
<command>mapproxy-util <replaceable>subcommand</replaceable></command>
      </screen>
    </para>
    <para>
      Each sub-command provides additional information:
      <screen>
<command>mapproxy-util <replaceable>subcommand</replaceable></command> <option>--help</option>
      </screen>
    </para>
    <para>
      The current sub-commands are:
      <itemizedlist>
        <listitem>
          <para>create</para>
        </listitem>
        <listitem>
          <para>serve-develop</para>
        </listitem>
        <listitem>
          <para>serve-multiapp-develop</para>
        </listitem>
        <listitem>
          <para>scales</para>
        </listitem>
        <listitem>
          <para>wms-capabilities</para>
        </listitem>
        <listitem>
          <para>grids</para>
        </listitem>
        <listitem>
          <para>export</para>
        </listitem>
        <listitem>
          <para>
            autoconfig (see 
            <citerefentry>
              <refentrytitle>mapproxy-util-autoconfig</refentrytitle>
              <manvolnum>1</manvolnum>
            </citerefentry>)
          </para>
        </listitem>
      </itemizedlist>
    </para>
  </refsect1>

  <refsect1 id='create'>
    <title>create</title>
    <para>
      This sub-command creates example configurations for you.
      There are templates for each configuration file.
    </para>
    <variablelist>

      <varlistentry>
        <term><option>-l</option></term>
        <term><option>--list-templates</option></term>
        <listitem>
          <para>
            List names of all available configuration templates.
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-t</option> <replaceable>name</replaceable></term>
        <term><option>--template</option> <replaceable>name</replaceable></term>
        <listitem>
          <para>
            Create a configuration with the named template.
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-f</option> <replaceable>mapproxy.yaml</replaceable></term>
        <term><option>--mapproxy-conf</option> <replaceable>mapproxy.yaml</replaceable></term>
        <listitem>
          <para>
            The path to the MapProxy configuration.
            Required for some templates.
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>--force</option></term>
        <listitem>
          <para>
            Overwrite any existing configuration with the same output filename.
          </para>
        </listitem>
      </varlistentry>

    </variablelist>

    <refsect2 id='create-configuration'>
      <title>Configuration templates</title>
      <para>
        Available templates are:
      </para>
      <variablelist>

        <varlistentry>
          <term>base-config:</term>
          <listitem>
            <para>
              Creates an example <filename>mapproxy.yaml</filename> and
              <filename>seed.yaml</filename> file.
              You need to pass the destination directory to the command.
            </para>
          </listitem>
        </varlistentry>

        <varlistentry>
          <term>log-ini:</term>
          <listitem>
            <para>
              Creates an example logging configuration.
              You need to pass the target filename to the command.
            </para>
          </listitem>
        </varlistentry>

        <varlistentry>
          <term>wsgi-app:</term>
          <listitem>
            <para>
              Creates an example server script for the given MapProxy
              configuration
              (<option>--f</option>/<option>--mapproxy-conf</option>).
              You need to pass the target filename to the command.
            </para>
          </listitem>
        </varlistentry>

      </variablelist>
    </refsect2>

    <refsect2 id='create-example'>
      <title>Example</title>
      <screen>
<command>mapproxy-util create</command> <option>-t</option> base-config ./
      </screen>
    </refsect2>

  </refsect1>

  <refsect1 id='serve-develop'>
    <title>serve-develop</title>
    <para>
      This sub-command starts a MapProxy instance of your configuration as a
      stand-alone server.
    </para>
    <para>
      You need to pass the MapProxy configuration as an argument.
      The server will automatically reload if you change the configuration or
      any of the MapProxy source code.
    </para>
    <variablelist>
      <varlistentry>
        <term><option>-b</option> <replaceable>address</replaceable></term>
        <term><option>--bind</option> <replaceable>address</replaceable></term>
        <listitem>
          <para>
            The server address where the HTTP server should listen for incoming
            connections.
            Can be a port (<literal>:8080</literal>),
            a host (<literal>localhost</literal>)
            or both (<literal>localhost:8081</literal>).
            The default is <literal>localhost:8080</literal>.
            You need to use <literal>0.0.0.0</literal> to be able to connect to
            the server from external clients.
          </para>
        </listitem>
      </varlistentry>
    </variablelist>

    <refsect2 id='serve-develop-example'>
      <title>Example</title>
      <screen>
<command>mapproxy-util serve-develop</command> ./mapproxy.yaml
      </screen>
    </refsect2>

  </refsect1>

  <refsect1 id='serve-multiapp-develop'>
    <title>serve-multiapp-develop</title>
    <para>
      This sub-command is similar to <command>serve-develop</command> but it
      starts a <literal>MultiMapProxy</literal> instance.
    </para>
    <para>
      You need to pass a directory of your MapProxy configurations as an
      argument.
      The server will automatically reload if you change any configuration or
      any of the MapProxy source code.
    </para>
    <variablelist>
      <varlistentry>
        <term><option>-b</option> <replaceable>address</replaceable></term>
        <term><option>--bind</option> <replaceable>address</replaceable></term>
        <listitem>
          <para>
            The server address where the HTTP server should listen for incoming
            connections.
            Can be a port (<literal>:8080</literal>),
            a host (<literal>localhost</literal>)
            or both (<literal>localhost:8081</literal>).
            The default is <literal>localhost:8080</literal>.
            You need to use <literal>0.0.0.0</literal> to be able to connect to
            the server from external clients.
          </para>
        </listitem>
      </varlistentry>
    </variablelist>

    <refsect2 id='serve-multiapp-develop-example'>
      <title>Example</title>
      <screen>
<command>mapproxy-util serve-multiapp-develop</command> my_projects/        
      </screen>
    </refsect2>

  </refsect1>

  <refsect1 id='scales'>
    <title>scales</title>
    <para>
      This sub-command helps to convert between scales and resolutions.
    </para>
    <para>
      Scales are ambiguous when the resolution of the output device (LCD,
      printer, mobile, etc) is unknown and therefore MapProxy only uses
      resolutions for configuration.
      You can use the <command>scales</command> sub-command to calculate
      between known scale values and resolutions.
    </para>
    <para>
      The command takes a list with one or more scale values and returns the
      corresponding resolution value.
    </para>
    <variablelist>

      <varlistentry>
        <term><option>--unit</option> &lt;m|d&gt;</term>
        <listitem>
          <para>
            Return resolutions in this unit per pixel (default meter per pixel).
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-l</option> <replaceable>n</replaceable></term>
        <term><option>--levels</option> <replaceable>n</replaceable></term>
        <listitem>
          <para>
            Calculate resolutions for <replaceable>n</replaceable> levels.
            This will double the resolution of the last scale value if
            <replaceable>n</replaceable> is larger than the number of the
            provided scales.
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-d</option> <replaceable>dpi</replaceable></term>
        <term><option>--dpi</option> <replaceable>dpi</replaceable></term>
        <listitem>
          <para>
            The resolution of the output display to use for the calculation.
            You need to set this to the same value of the client/server
            software you are using.
            Common values are 72 and 96.
            The default value is the equivalent of a pixel size of .28mm,
            which is around 91 DPI.
            This is the value the OGC uses since the WMS 1.3.0 specification.
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>--as-res-config</option></term>
        <listitem>
          <para>
            Format the output so that it can be pasted into a MapProxy grid
            configuration.
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>--res-to-scale</option></term>
        <listitem>
          <para>
            Calculate from resolutions to scale.
          </para>
        </listitem>
      </varlistentry>

    </variablelist>

    <refsect2 id='scales-example'>
      <title>Example</title>
      <para>
        For multiple levels as MapProxy configuration snippet:
        <screen>
<command>mapproxy-util scales</command> <option>-l</option> 4 <option>--as-res-config</option> 100000
        </screen>
        <screen>
res: [
     #  res            level        scale
       28.0000000000, #  0      100000.00000000
       14.0000000000, #  1       50000.00000000
        7.0000000000, #  2       25000.00000000
        3.5000000000, #  3       12500.00000000
]
        </screen>
      </para>
      <para>
        With multiple scale values and custom DPI:
        <screen>
<command>mapproxy-util scales</command> <option>--dpi</option> 96 <option>--as-res-config</option> \
    100000 50000 25000 10000
        </screen>
        <screen>
res: [
     #  res            level        scale
       26.4583333333, #  0      100000.00000000
       13.2291666667, #  1       50000.00000000
        6.6145833333, #  2       25000.00000000
        2.6458333333, #  3       10000.00000000
]
        </screen>
      </para>
    </refsect2>

  </refsect1>

  <refsect1 id='wms-capabilities'>
    <title>wms-capabilities</title>
    <para>
      This sub-command parses a valid capabilities document from a URL and
      displays all available layers.
    </para>
    <para>
      This tool does not create a MapProxy configuration, but the output
      should help you to set up or modify your MapProxy configuration.
    </para>
    <para>
      The command takes a valid URL GetCapabilities URL.
    </para>
    <variablelist>

      <varlistentry>
        <term><option>--host</option> <replaceable>URL</replaceable></term>
        <listitem>
          <para>
            Display all available Layers for this service.
            Each new layer will be marked with a hyphen and all sublayers are
            indented.
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>--version</option> <replaceable>versionnumber</replaceable></term>
        <listitem>
          <para>
            Parse the Capabilities-document for the given version.
            Only version 1.1.1 and 1.3.0 are supported.
            The default value is 1.1.1.
          </para>
        </listitem>
      </varlistentry>

    </variablelist>

    <refsect2 id='wms-capabilities-example'>
      <title>Example</title>
      <para>
        With the following MapProxy layer configuration:
      </para>
      <screen>
layers:
  - name: osm
    title: Omniscale OSM WMS - osm.omniscale.net
    sources: [osm_cache]
  - name: foo
    title: Group Layer
    layers:
      - name: layer1a
        title: Title of Layer 1a
        sources: [osm_cache]
      - name: layer1b
        title: Title of Layer 1b
        sources: [osm_cache]
      </screen>
      <para>
        Parsed capabilities document:
      </para>
      <screen>
<command>mapproxy-util wms-capabilities</command> http://127.0.0.1:8080/service?REQUEST=GetCapabilities
      </screen>
      <screen>
Capabilities Document Version 1.1.1
Root-Layer:
  - title: MapProxy WMS Proxy
    url: http://127.0.0.1:8080/service?
    opaque: False
    srs: ['EPSG:31467', 'EPSG:31466', 'EPSG:4326', 'EPSG:25831', 'EPSG:25833',
          'EPSG:25832', 'EPSG:31468', 'EPSG:900913', 'CRS:84', 'EPSG:4258']
    bbox:
        EPSG:900913: [-20037508.3428, -20037508.3428, 20037508.3428, 20037508.3428]
        EPSG:4326: [-180.0, -85.0511287798, 180.0, 85.0511287798]
    queryable: False
    llbbox: [-180.0, -85.0511287798, 180.0, 85.0511287798]
    layers:
      - name: osm
        title: Omniscale OSM WMS - osm.omniscale.net
        url: http://127.0.0.1:8080/service?
        opaque: False
        srs: ['EPSG:31467', 'EPSG:31466', 'EPSG:25832', 'EPSG:25831', 'EPSG:25833',
              'EPSG:4326', 'EPSG:31468', 'EPSG:900913', 'CRS:84', 'EPSG:4258']
        bbox:
            EPSG:900913: [-20037508.3428, -20037508.3428, 20037508.3428, 20037508.3428]
            EPSG:4326: [-180.0, -85.0511287798, 180.0, 85.0511287798]
        queryable: False
        llbbox: [-180.0, -85.0511287798, 180.0, 85.0511287798]
      - name: foobar
        title: Group Layer
        url: http://127.0.0.1:8080/service?
        opaque: False
        srs: ['EPSG:31467', 'EPSG:31466', 'EPSG:25832', 'EPSG:25831', 'EPSG:25833',
              'EPSG:4326', 'EPSG:31468', 'EPSG:900913', 'CRS:84', 'EPSG:4258']
        bbox:
            EPSG:900913: [-20037508.3428, -20037508.3428, 20037508.3428, 20037508.3428]
            EPSG:4326: [-180.0, -85.0511287798, 180.0, 85.0511287798]
        queryable: False
        llbbox: [-180.0, -85.0511287798, 180.0, 85.0511287798]
        layers:
          - name: layer1a
            title: Title of Layer 1a
            url: http://127.0.0.1:8080/service?
            opaque: False
            srs: ['EPSG:31467', 'EPSG:31466', 'EPSG:25832', 'EPSG:25831', 'EPSG:25833',
                  'EPSG:4326', 'EPSG:31468', 'EPSG:900913', 'CRS:84', 'EPSG:4258']
            bbox:
                EPSG:900913: [-20037508.3428, -20037508.3428, 20037508.3428, 20037508.3428]
                EPSG:4326: [-180.0, -85.0511287798, 180.0, 85.0511287798]
            queryable: False
            llbbox: [-180.0, -85.0511287798, 180.0, 85.0511287798]
          - name: layer1b
            title: Title of Layer 1b
            url: http://127.0.0.1:8080/service?
            opaque: False
            srs: ['EPSG:31467', 'EPSG:31466', 'EPSG:25832', 'EPSG:25831', 'EPSG:25833',
                  'EPSG:4326', 'EPSG:31468', 'EPSG:900913', 'CRS:84', 'EPSG:4258']
            bbox:
                EPSG:900913: [-20037508.3428, -20037508.3428, 20037508.3428, 20037508.3428]
                EPSG:4326: [-180.0, -85.0511287798, 180.0, 85.0511287798]
            queryable: False
            llbbox: [-180.0, -85.0511287798, 180.0, 85.0511287798]
      </screen>
    </refsect2>

  </refsect1>

  <refsect1 id='grids'>
    <title>grids</title>
    <para>
      This sub-command displays information about configured grids.
    </para>
    <para>
      The command takes a MapProxy configuration file and returns all
      configured grids.
    </para>
    <para>
      Furthermore, default values for each grid will be displayed if they are
      not defined explicitly.
      All default values are marked with an asterisk in the output.
    </para>
    <variablelist>

      <varlistentry>
        <term><option>-f</option> <replaceable>path/to/config</replaceable></term>
        <term><option>--mapproxy-config</option> <replaceable>path/to/config</replaceable></term>
        <listitem>
          <para>
            Display all configured grids for this MapProxy configuration with
            detailed information.
            If this option is not set, the sub-command will try to use the last
            argument as the mapproxy config.
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-l</option></term>
        <term><option>--list</option></term>
        <listitem>
          <para>
            Display only the names of the grids for the given configuration,
            which are used by any grid.
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>--all</option></term>
        <listitem>
          <para>
            Show also grids that are not referenced by any cache.
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-g</option> <replaceable>grid_name</replaceable></term>
        <term><option>--grid</option> <replaceable>grid_name</replaceable></term>
        <listitem>
          <para>
            Display information only for a single grid.
            The tool will exit, if the grid name is not found.
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-c</option> <replaceable>coverage name</replaceable></term>
        <term><option>--coverage</option> <replaceable>coverage name</replaceable></term>
        <listitem>
          <para>
            Display an approximation of the number of tiles for each level that
            are within this coverage.
            The coverage must be defined in Seed configuration.
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-s</option> <replaceable>seed.yaml</replaceable></term>
        <term><option>--seed-conf</option> <replaceable>seed.yaml</replaceable></term>
        <listitem>
          <para>
            This option loads the seed configuration and is needed if you use
            the <option>--coverage</option> option.
          </para>
        </listitem>
      </varlistentry>

    </variablelist>

    <refsect2 id='grids-example'>
      <title>Example</title>
      <para>
        With the following MapProxy grid configuration:
      </para>
      <screen>
grids:
  localgrid:
    srs: EPSG:31467
    bbox: [5,50,10,55]
    bbox_srs: EPSG:4326
    min_res: 10000
  localgrid2:
    base: localgrid
    srs: EPSG:25832
    res_factor: sqrt2
    tile_size: [512, 512]
      </screen>
      <para>
        List all configured grids:
      </para>
      <screen>
<command>mapproxy-util grids</command> <option>--list</option> <option>--mapproxy-config</option> /path/to/mapproxy.yaml
      </screen>
      <screen>
GLOBAL_GEODETIC
GLOBAL_MERCATOR
localgrid
localgrid2
      </screen>
      <para>
        Display detailed information for one specific grid:
      </para>
      <screen>
<command>mapproxy-util grids</command> <option>--grid</option> localgrid <option>--mapproxy-conf</option> /path/to/mapproxy.yaml
      </screen>
      <screen>
localgrid:
    Configuration:
        bbox: [5, 50, 10, 55]
        bbox_srs: 'EPSG:4326'
        min_res: 10000
        origin*: 'sw'
        srs: 'EPSG:31467'
        tile_size*: [256, 256]
    Levels: Resolutions, # x * y = total tiles
        00:  10000,             #      1 * 1      =        1
        01:  5000.0,            #      1 * 1      =        1
        02:  2500.0,            #      1 * 1      =        1
        03:  1250.0,            #      2 * 2      =        4
        04:  625.0,             #      3 * 4      =       12
        05:  312.5,             #      5 * 8      =       40
        06:  156.25,            #      9 * 15     =      135
        07:  78.125,            #     18 * 29     =      522
        08:  39.0625,           #     36 * 57     =   2.052K
        09:  19.53125,          #     72 * 113    =   8.136K
        10:  9.765625,          #    144 * 226    =  32.544K
        11:  4.8828125,         #    287 * 451    = 129.437K
        12:  2.44140625,        #    574 * 902    = 517.748K
        13:  1.220703125,       #   1148 * 1804   =   2.071M
        14:  0.6103515625,      #   2295 * 3607   =   8.278M
        15:  0.30517578125,     #   4589 * 7213   =  33.100M
        16:  0.152587890625,    #   9178 * 14426  = 132.402M
        17:  0.0762939453125,   #  18355 * 28851  = 529.560M
        18:  0.03814697265625,  #  36709 * 57701  =   2.118G
        19:  0.019073486328125, #  73417 * 115402 =   8.472G
      </screen>
    </refsect2>

  </refsect1>

  <refsect1 id='export'>
    <title>export</title>
    <para>
      This sub-command exports tiles from one cache to another.
      This is similar to the seed tool, but you don't need to edit the
      configuration.
      The destination cache, grid and the coverage can be defined on the
      command line.
    </para>
    <para>
      Required arguments:
    </para>
    <variablelist>

      <varlistentry>
        <term><option>-f</option> <replaceable>path</replaceable></term>
        <term><option>--mapproxy-conf</option> <replaceable>path</replaceable></term>
        <listitem>
          <para>
            The path to the MapProxy configuration of the source cache.
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>--source</option> <replaceable>name</replaceable></term>
        <listitem>
          <para>
            Name of the source or cache to export.
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>--levels</option> <replaceable>list</replaceable></term>
        <listitem>
          <para>
            Comma separated list of levels to export.
            You can also define a range of levels.
            For example <literal>'1,2,3,4,5'</literal>,
            <literal>'1..10'</literal> or <literal>'1,3,4,6..8'</literal>.
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>--grid</option> <replaceable>grid</replaceable></term>
        <listitem>
          <para>
            The tile grid for the export.
            The option can either be the name of the grid as defined in the in
            the MapProxy configuration, or it can be the grid definition itself.
            You can define a grid as a single string of the key-value pairs.
            The grid definition supports all grid parameters.
            See below for examples.
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>--dest</option> <replaceable>destination</replaceable></term>
        <listitem>
          <para>
            Destination of the export.
            Can be a filename, directory or URL, depending on the export
            <option>--type</option>.
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>--type</option> <replaceable>type</replaceable></term>
        <listitem>
          <para>
            Choose the export type.
            See below for a list of all options.
          </para>
        </listitem>
      </varlistentry>

    </variablelist>
    <para>
      Other options:
    </para>
    <variablelist>

      <varlistentry>
        <term><option>--fetch-missing-tiles</option></term>
        <listitem>
          <para>
            If MapProxy should request missing tiles from the source.
            By default, the export tool will only existing tiles.
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>--coverage</option> <replaceable>coverage</replaceable></term>
        <term><option>--srs</option> <replaceable>srs</replaceable></term>
        <term><option>--where</option> <replaceable>where</replaceable></term>
        <listitem>
          <para>
            Limit the export to this coverage.
            You can use a BBOX, WKT files or OGR datasources.
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-c</option> <replaceable>N</replaceable></term>
        <term><option>--concurrency</option> <replaceable>N</replaceable></term>
        <listitem>
          <para>
            The number of concurrent export processes.
          </para>
        </listitem>
      </varlistentry>

    </variablelist>

    <refsect2 id='export-types'>
      <title>Export types</title>
      <variablelist>

        <varlistentry>
          <term>tms:</term>
          <listitem>
            <para>
              Export tiles in a TMS like directory structure.
            </para>
          </listitem>
        </varlistentry>

        <varlistentry>
          <term>mapproxy</term>
          <term>tc</term>
          <listitem>
            <para>
              Export tiles like the internal cache directory structure.
              This is compatible with TileCache.
            </para>
          </listitem>
        </varlistentry>

        <varlistentry>
          <term>mbtile</term>
          <listitem>
            <para>
              Exports tiles into a MBTile file.
            </para>
          </listitem>
        </varlistentry>

        <varlistentry>
          <term>arcgis</term>
          <listitem>
            <para>
              Exports tiles in a ArcGIS exploded cache directory structure.
            </para>
          </listitem>
        </varlistentry>

      </variablelist>
    </refsect2>

    <refsect2 id='export-example'>
      <title>Example</title>
      <para>
        Export tiles into a TMS directory structure under
        <filename>./cache/</filename>.
        Limit export to the BBOX and levels 0 to 6.
      </para>
      <screen>
<command>mapproxy-util export</command> <option>-f</option> mapproxy.yaml <option>--grid</option> osm_grid \
    <option>--source</option> osm_cache <option>--dest</option> ./cache/ \
    <option>--levels</option> 1..6 <option>--coverage</option> 5,50,10,60 <option>--srs</option> 4326
      </screen>
      <para>
        Export tiles into an MBTiles file.
        Limit export to a shape coverage.
      </para>
      <screen>
<command>mapproxy-util export</command> <option>-f</option> mapproxy.yaml <option>--grid</option> osm_grid \
    <option>--source</option> osm_cache <option>--dest</option> osm.mbtiles <option>--type</option> mbtile \
    <option>--levels</option> 1..6 <option>--coverage</option> boundaries.shp \
    <option>--where</option> 'CNTRY_NAME = "Germany"' <option>--srs</option> 3857
      </screen>
      <para>
        Export tiles into an MBTiles file using a custom grid definition.
      </para>
      <screen>
<command>mapproxy-util export</command> <option>-f</option> mapproxy.yaml <option>--levels</option> 1..6 \
    <option>--grid</option> "srs='EPSG:4326' bbox=[5,50,10,60] tile_size=[512,512]" \
    <option>--source</option> osm_cache <option>--dest</option> osm.mbtiles <option>--type</option> mbtile
      </screen>
    </refsect2>

  </refsect1>

</refentry>
